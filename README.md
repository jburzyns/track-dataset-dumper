# track-dataset-dumper

Welcome to the track dumpster. This is a package to dump h5 files of tracks from
(D)AODs.  It is inspired by and heavily borrows from the [FTAG
dumpster](https://gitlab.cern.ch/atlas-flavor-tagging-tools/training-dataset-dumper).
All credit goes to the developers of that package. 

## setup

The usual deal:

```
git clone ssh://git@gitlab.cern.ch:7999/jburzyns/track-dataset-dumper.git
mkdir build
cd build
asetup Athena,master,latest
cmake ../track-dataset-dumper
make
cd ..
source build/x*/setup.sh
```

## running

Use the `dump-track` executable:

```
dump-tracks -c [config file] [input file]
```