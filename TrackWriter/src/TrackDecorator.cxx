#include "TrackDecorator.hh"

// the constructor just builds the decorators
TrackDecorator::TrackDecorator(const std::string& prefix):
  m_deco_d0(prefix + "d0"),
  m_deco_z0(prefix + "z0"),
  m_deco_phi0(prefix + "phi0"),
  m_deco_theta(prefix + "theta"),
  m_deco_qOverP(prefix + "qOverP"),
  m_deco_d0_err(prefix + "d0_err"),
  m_deco_z0_err(prefix + "z0_err"),
  m_deco_phi0_err(prefix + "phi0_err"),
  m_deco_theta_err(prefix + "theta_err"),
  m_deco_qOverP_err(prefix + "qOverP_err")
{
}

void TrackDecorator::decorate(const xAOD::TrackParticle& track) const {

  float d0 = track.d0();
  float z0 = track.z0();
  float phi0 = track.phi0();
  float theta = track.theta();
  float qOverP = track.qOverP();

  float d0_err = track.definingParametersCovMatrix()(0,0);
  float z0_err = track.definingParametersCovMatrix()(1,1);
  float phi0_err = track.definingParametersCovMatrix()(2,2);
  float theta_err = track.definingParametersCovMatrix()(3,3);
  float qOverP_err = track.definingParametersCovMatrix()(4,4);

  m_deco_d0(track) = d0;
  m_deco_z0(track) = z0;
  m_deco_phi0(track) = phi0;
  m_deco_theta(track) = theta;
  m_deco_qOverP(track) = qOverP;

  m_deco_d0_err(track) = d0_err;
  m_deco_z0_err(track) = z0_err;
  m_deco_phi0_err(track) = phi0_err;
  m_deco_theta_err(track) = theta_err;
  m_deco_qOverP_err(track) = qOverP_err;

}
