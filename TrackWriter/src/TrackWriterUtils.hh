#ifndef TRACK_WRITER_UTILS_H
#define TRACK_WRITER_UTILS_H

#include "HDF5Utils/Writer.h"

#include <string>
#include <vector>
#include <map>

namespace xAOD {
  class TrackParticle_v1;
  typedef TrackParticle_v1 TrackParticle;
  class EventInfo_v1;
  typedef EventInfo_v1 EventInfo;
}

// structure to serve as base for lambda functions
struct TrackOutputs {
  TrackOutputs();
  const xAOD::TrackParticle* track;
  const xAOD::EventInfo* event_info;
};

class TrackConfig;

using TrackConsumers = H5Utils::Consumers<const TrackOutputs&>;
using FloatFiller = std::function<float(const TrackOutputs&)>;

void add_track_variables(TrackConsumers&, 
                         const TrackConfig& cfg,
                         H5Utils::Compression half_prec);

void add_track_int_variables(TrackConsumers&,
                             const std::vector<std::string>&);
void add_event_info(TrackConsumers&,
                    const std::vector<std::string>&,
                    H5Utils::Compression);

void add_track_float_fillers(TrackConsumers&,
                             const std::vector<std::string>&,
                             float default_value,
                             H5Utils::Compression compression);


class ITrackOutputWriter
{
public:
  virtual ~ITrackOutputWriter() = default;
  virtual void fill(const std::vector<TrackOutputs>& jo) = 0;
  virtual void flush() = 0;
};

class TrackOutputWriter: public ITrackOutputWriter
{
private:
  H5Utils::Writer<0,const TrackOutputs&> m_writer;
public:
  TrackOutputWriter(H5::Group& file,
                  const std::string& name,
                  const TrackConsumers& cons);
  void fill(const std::vector<TrackOutputs>& jos) override;
  void flush() override;
};

class TrackOutputWriter1D: public ITrackOutputWriter
{
private:
  H5Utils::Writer<1,const TrackOutputs&> m_writer;
public:
  TrackOutputWriter1D(H5::Group& file,
                    const std::string& name,
                    const TrackConsumers& cons,
                    size_t n_tracks);
  void fill(const std::vector<TrackOutputs>& jos) override;
  void flush() override;
};



#endif
