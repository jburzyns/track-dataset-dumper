#ifndef TRACK_TRUTH_DECORATOR_HH
#define TRACK_TRUTH_DECORATOR_HH

#include "xAODTracking/TrackParticle.h"
#include "xAODTracking/TrackParticleContainer.h"

#include "xAODTruth/TruthVertexContainer.h"
#include "xAODTruth/TruthEventBaseContainer.h"

#include <string>

// this is a bare-bones example of a class that could be used to
// decorate a track with additional information

class TrackTruthDecorator
{
public:

  TrackTruthDecorator(const std::string& decorator_prefix = "example_");

  // this is the function that actually does the decoration
  void decorate(const xAOD::TrackParticle& track, 
                std::vector<const xAOD::TruthVertex*>& truthVertices) const;

private:
  // All this class does is apply a decoration, so all it needs to do
  // is contain one decorator. We could have also written a function
  // and statically initalized this, but static data in functions is
  // probably best avoided.

  // truth labels
  SG::AuxElement::Decorator<int> m_deco_truthPVIndex;

};

#endif
