#ifndef TRACK_TOOLS_HH
#define TRACK_TOOLS_HH

#include "TrackDecorator.hh"
#include "TrackTruthDecorator.hh"

class TrackConfig;

struct TrackTools {

  TrackTools(const TrackConfig&);
  ~TrackTools();

  // decorators
  TrackDecorator trkDecorator;
  TrackTruthDecorator trkTruthDecorator;

};

#endif
