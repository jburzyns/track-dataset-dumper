#include "TrackConfig.hh"
#include "ConfigFileTools.hh"

#define BOOST_BIND_GLOBAL_PLACEHOLDERS // ignore deprecated ptree issues
#include <boost/property_tree/ptree.hpp>
#include <boost/property_tree/json_parser.hpp>
#include <boost/optional/optional.hpp>

#include <filesystem>
#include <set>

std::vector<std::string> get(const VariableList& v, const std::string& k) {
  if (v.count(k)) return v.at(k);
  return {};
}

const nlohmann::ordered_json get_merged_json(const std::filesystem::path& cfg_path)
{
  namespace fs = std::filesystem;
  namespace cft = ConfigFileTools;
  if (!fs::exists(cfg_path)) {
    throw std::runtime_error(cfg_path.string() + " doesn't exist");
  }
  std::ifstream cfg_stream(cfg_path);
  auto nlocfg = nlohmann::ordered_json::parse(cfg_stream);
  cft::combine_files(nlocfg, cfg_path.parent_path());
  return nlocfg;
}

TrackConfig get_track_config(const std::filesystem::path& cfg_path)
{
  auto nlocfg = get_merged_json(cfg_path);
  return get_track_config(nlocfg, cfg_path.stem());
}

TrackConfig get_track_config(const nlohmann::ordered_json& nlocfg,
                             const std::string& tool_prefix)
{

  (void) tool_prefix;
  namespace pt = boost::property_tree;
  namespace cft = ConfigFileTools;
  using OJ = nlohmann::ordered_json;
  using VOJ = std::vector<OJ>;

  ConfigFileTools::OptionalConfigurationObject opt_nlocfg(nlocfg);

  TrackConfig config;
  config.track_collection = opt_nlocfg.require<std::string>("track_collection");

  const auto& nlo_variables = opt_nlocfg.require<OJ>("variables");
  auto variables = cft::from_nlohmann(nlo_variables);

  config.variables = cft::get_variable_list(variables.get_child("tracks"));

  return config;
}
