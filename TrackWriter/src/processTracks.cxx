#include "processTracks.hh"
#include "TrackDataset.hh"
#include "TrackConfig.hh"
#include "TrackTools.hh"

#include "xAODRootAccess/TEvent.h"
#include "xAODTruth/TruthParticleContainer.h"
#include "xAODTruth/TruthEventContainer.h"
#include "xAODTruth/TruthEventBaseContainer.h"
#include "xAODTruth/TruthPileupEventContainer.h"
#include "xAODTruth/TruthVertexContainer.h"
#include "xAODTracking/TrackParticleContainer.h"
#include "xAODEventInfo/EventInfo.h"

namespace {
  void check_rc(StatusCode code) {
    if (!code.isSuccess()) throw std::runtime_error("bad return code");
  }
}

void processTracks(xAOD::TEvent& event, 
                  const TrackConfig& jobcfg,
                  const TrackTools& tools,
                  TrackDataset& out) {

  const xAOD::EventInfo *event_info = nullptr;
  check_rc( event.retrieve(event_info, "EventInfo") );

  // read the tracks
  const xAOD::TrackParticleContainer *tracks = nullptr;

  check_rc( event.retrieve(tracks, jobcfg.track_collection) );

  // Identify MC vertices to match to
  std::vector<const xAOD::TruthEventBaseContainer *> truthContainers;
  std::vector<const xAOD::TruthVertex *> truthVertices;

  const xAOD::TruthEventBaseContainer * truthEvents = nullptr;
  const xAOD::TruthEventBaseContainer * truthPileup = nullptr;
  check_rc( event.retrieve( truthEvents, "TruthEvents" ) );
  check_rc( event.retrieve( truthPileup, "TruthPileupEvents" ) );

  truthContainers.push_back( truthEvents );
  truthContainers.push_back( truthPileup );

  for(const auto truthContainer : truthContainers) {
    for(const auto truthEvent : *truthContainer) {
      const auto truthVertex = truthEvent->truthVertexLink(0);
      truthVertices.push_back(*truthVertex);
    }
  }

  std::vector<const xAOD::TrackParticle*> tracks_to_write;
  for (const xAOD::TrackParticle* track: *tracks) {

    tools.trkDecorator.decorate(*track);
    tools.trkTruthDecorator.decorate(*track, truthVertices);

    tracks_to_write.push_back(track);
  } // end loop over tracks

  // write out tracks
  if (!tracks_to_write.empty()) {
    out.track_writer.write(tracks_to_write, event_info);
  }

}
