#include "TrackTruthDecorator.hh"
#include "xAODTruth/TruthParticle.h"
#include "xAODTruth/TruthVertex.h"


// the constructor just builds the decorators
TrackTruthDecorator::TrackTruthDecorator(const std::string& prefix):
  m_deco_truthPVIndex(prefix + "truthPVIndex")
{
}

void TrackTruthDecorator::decorate(const xAOD::TrackParticle& track,
                                   std::vector<const xAOD::TruthVertex*>& truthVertices ) const 
{

  int index = -999; // fake/no PV match

  typedef ElementLink<xAOD::TruthParticleContainer> ElementTruthLink_t;
  static const SG::AuxElement::ConstAccessor< ElementTruthLink_t > acc( "truthParticleLink" );

  auto linkedTruth = acc(track);
  
  if(!linkedTruth.isValid()) return;

  // loop over truth containers
  for(const auto PV : truthVertices) {
    for(const auto &outgoingPart : PV->outgoingParticleLinks()) {
      if(!outgoingPart.isValid()) continue;     

      // if this truth particle is matched to our track, save the PV index
      if((*outgoingPart)->barcode() == (*linkedTruth)->barcode()) {
        index = PV->index();
      }
    }
  }

  m_deco_truthPVIndex(track) = index;
}
