#
# Cmake for TrackWriter
#

# Set the name of the package:
atlas_subdir( TrackWriter )

# External(s) used by the package:
find_package(HDF5 1.10.1 REQUIRED COMPONENTS CXX C)
find_package(nlohmann_json REQUIRED)
find_package(Boost REQUIRED)

# We don't want any warnings in compilation
add_compile_options(-Werror)

# We have to define the libraries first because we might not use
# some. This lets us add them condionally.
set(LINK_LIBRARIES
  xAODRootAccess
  xAODTracking
  xAODTruth
  HDF5Utils
  AsgTools
  PathResolver
  MCTruthClassifierLib
  )

# common requirements
atlas_add_library(dataset-dumper
  src/*.cxx
  PUBLIC_HEADERS src
  LINK_LIBRARIES ${LINK_LIBRARIES}
  )

# Build the executables
atlas_add_executable( dump-tracks
  util/dump-tracks.cxx
  )
target_link_libraries( dump-tracks dataset-dumper)
target_include_directories( dump-tracks PRIVATE src)

